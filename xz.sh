#!/bin/bash
PROCESSES="ttnode ttmanager happ eds lsnode bynode ipes wsv qynode"
for PROCESS in $PROCESSES
do
    echo "开始杀死所有带有'$PROCESS'的进程..."
    PIDS=$(ps aux | grep "$PROCESS" | awk '{print $2}')
    if [ -n "$PIDS" ]; then
        for PID in $PIDS
        do
            if ps -p $PID > /dev/null; then
               kill -9 $PID
               echo "进程 $PID 已被杀死。"
            else
               echo "没有找到进程 $PID。"
            fi
        done
    else
        echo "没有找到带有'$PROCESS'的进程。"
    fi
    sleep 3
done

TTMANAGER=$(ls /usr/ttnode | grep -E 'ttmanager' | grep -v '.bak$')
if [ -n "$TTMANAGER" ]; then
    echo "开始卸载$TTMANAGER..."
    /usr/ttnode/$TTMANAGER -uninstall
    echo "$TTMANAGER已卸载。"
    sleep 3
else
    echo "在/usr/ttnode文件夹中没有找到ttmanager文件。"
fi

echo "开始删除/usr/ttnode文件夹..."
rm -rf /usr/ttnode
rm -rf /root/.qynode_cacheFile
echo "/usr/ttnode文件夹已删除。"

echo "开始删除by相关文件..."
FILES="/usr/local/by-agent /usr/local/by-daemon /usr/local/by-watcher /usr/local/bin/ttagent"
for FILE in $FILES
do
    if [ -e "$FILE" ]; then
        rm -rf "$FILE"
        echo "已删除文件或文件夹 $FILE。"
    else
        echo "没有找到文件或文件夹 $FILE。"
    fi
done
sed -i '/nohup/d' /etc/rc.local
echo "*****卸载脚本不一定能完全杀掉进程，所以杀完进程后记得重启一下"
