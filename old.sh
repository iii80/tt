#!/bin/bash

# 提示用户选择部署类型
echo "请选择部署类型："
echo "1. 二进制部署"
echo "2. Docker部署"
read -p "请输入选项数字: " deployment_option

# 根据用户选择执行不同的部署方式
case $deployment_option in
    1)
        echo "你选择了二进制部署。"

        # 定义下载链接
        declare -A download_link
        download_link=(["arm32"]="https://tiptime-api.com/cdn/ttmanager2/1.5.12/ttmanager_arm32" ["arm64"]="https://tiptime-api.com/cdn/ttmanager2/1.5.12/ttmanager_arm64" ["amd64"]="https://tiptime-api.com/cdn/ttmanager2/1.5.12/ttmanager_amd64")

        # 获取系统架构
        architecture=$(uname -m)

        # 根据系统架构选择下载链接
        case $architecture in
            "armv7l")
                link=${download_link["arm32"]}
                ;;
            "aarch64")
                link=${download_link["arm64"]}
                ;;
            "x86_64")
                link=${download_link["amd64"]}
                ;;
            *)
                echo "不支持的系统架构: $architecture"
                exit 1
                ;;
        esac

        # 创建目录
        mkdir -p /usr/ttnode

        # 下载程序
        wget -O /usr/ttnode/ttmanager $link

        # 赋予程序权限
        chmod 777 /usr/ttnode/ttmanager

        # 执行程序
        /usr/ttnode/ttmanager -g
        # 检查是否已经安装了Docker
if ! command -v docker &> /dev/null
then
    echo "Docker未安装。你想现在安装Docker吗？注:BY业务需要docker (y/n)"
    read -e install_docker
    if [[ $install_docker == 'y' ]]
    then
        curl -fsSL https://get.docker.com -o get-docker.sh
        sh get-docker.sh
        echo -e '{\n"exec-opts": ["native.cgroupdriver=cgroupfs"]\n}' > /etc/docker/daemon.json
        systemctl restart docker
    else
        echo "Docker未安装，无法运行BYNode。"
        
    fi
fi

        # 提示用户选择节点类型
        echo "请选择要运行的节点类型："
        echo "1. qynode"
        echo "2. bynode"
        echo "3. edsnode"
        echo "4. lsnode"
        read -p "请输入选项数字: " node_type

        case $node_type in
            1)
                node="qynode"
                ;;
            2)
                node="bynode"
                ;;
            3)
                node="edsnode"
                ;;
            4)
                node="lsnode"
                ;;
            *)
                echo "无效的选项。"
                exit 1
                ;;
        esac

        # 提示用户输入目录
        while true; do
            read -e -p "请输入缓存目录: " dir
            if [[ -d $dir ]]; then
                break
            else
                echo "无效的目录，请重新输入。"
            fi
        done

        # 创建目录
        mkdir -p $dir/ttnode

        # 保存原来的 /etc/rc.local 文件内容
        original_content=$(cat /etc/rc.local)

        # 删除原来的 /etc/rc.local 文件
        rm /etc/rc.local

        # 创建新的 /etc/rc.local 文件
        touch /etc/rc.local

        # 将 shebang 写入新的 /etc/rc.local 文件
        echo "#!/bin/sh -e" >> /etc/rc.local

        # 将启动命令写入新的 /etc/rc.local 文件
        echo "nohup /usr/ttnode/ttrun.sh -c $dir/ttnode  > /dev/null 2>&1 &" >> /etc/rc.local

        # 将原来的 /etc/rc.local 文件内容（除了 shebang）追加到新的 /etc/rc.local 文件中
        echo "${original_content#*#!/bin/sh -e}" >> /etc/rc.local

        # 给 /etc/rc.local 文件添加执行权限
        chmod +x /etc/rc.local

        # 执行脚本
        nohup /usr/ttnode/ttrun.sh -c $dir/ttnode -t $node > /dev/null 2>&1 &
        # 显示启动命令
        echo "*
              *
              *
              *
              *           执行完脚本后甜程序已经启动,没有看到程序启动执行执行启动命令
              你的启动命令是: nohup /usr/ttnode/ttrun.sh -c $dir/ttnode -t $node > /dev/null 2>&1 &
                                    管理界面依然是ip:1024
                                    默认启动的业务是qynode
              设置了开机启动,启动命令在/etc/rc.local,如果不需要开机启动可以删掉这条命令
              有的系统rc.local可能不会执行，所以自行检查开机启动是否有效
              填写邀请码187099领取加成卡，有问题可以加q 3386673746解决"
        ;;
    2)
        echo "你选择了Docker部署。"

        # 检查是否已经安装了 Docker
        if ! command -v docker &> /dev/null
        then
            echo "Docker未安装。你想现在安装Docker吗？ (y/n)"
            read -e install_docker
            if [[ $install_docker == 'y' ]]
            then
                curl -fsSL https://get.docker.com -o get-docker.sh
                sh get-docker.sh
            else
                echo "Docker未安装，脚本将退出。"
                exit 1
            fi
        fi

        # 提示用户输入缓存目录
        while true; do
            echo "请输入缓存目录:"
            read -e cache_dir
            if [[ -d $cache_dir ]]; then
                break
            else
                echo "无效的目录，请重新输入。"
            fi
        done

        # 提示用户输入容器名称
        echo "请输入容器名称:"
        read -e container_name

        # 构建docker命令
        docker_cmd="docker run -d \
          -v ${cache_dir}:/mnt/data/ttnode \
          -v /var/run/docker.sock:/var/run/docker.sock \
          -v /proc:/host/proc:ro \
          --name ${container_name} \
          --hostname ttnode \
          --device /dev/mem \
          --cap-add SYS_RAWIO \
          --privileged \
          --net=host \
          --restart=always \
          -e container_name=${container_name} \
          registry.cn-hangzhou.aliyuncs.com/tiptime/ttnode:latest"

        # 显示docker命令
        echo "以下是完整的docker命令:"
        echo $docker_cmd
        # 显示启动命令
        echo "填写邀请码187099领取加成卡，有问题可以加q 3386673746解决
                       docker默认启动的业务是qynode
                        管理界面依然是设备ip：1024"

        # 执行docker命令
        eval $docker_cmd
        ;;
    *)
        echo "无效的选项。脚本将退出。"
        exit 1
        ;;
esac