# aarch64 x86_64

platform=$(uname -a)
case $platform in
    *x86_64*)
        url='amd64'
        ;;
    *x86*)
        url='x86'
        ;;
    *amd64*)
        url='amd64'
        ;;
    *armv7*)
        url='arm32'
        ;;
    *armv8*)
        url='arm64'
        ;;
    *arm64*)
        url='arm64'
        ;;
    *aarch64*)
        url='arm64'
        ;;
    *)
        url='arm32'
esac

echo $url

url='https://cos.tiptime.cn/ttmanager/1.3.14/ttmanager_'$url
curl --request GET -sL \
     --url $url\
     --output './ttmanager'
chmod +x ./ttmanager
docker cp ./ttmanager $1:/data/main && docker restart $1