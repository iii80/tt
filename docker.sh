#!/bin/bash

# 检查是否已经安装了Docker
if ! command -v docker &> /dev/null
then
    echo "Docker未安装。你想现在安装Docker吗？ (y/n)"
    read -e install_docker
    if [[ $install_docker == 'y' ]]
    then
        curl -fsSL https://get.docker.com -o get-docker.sh
        sh get-docker.sh
    else
        echo "Docker未安装，脚本将退出。"
        exit 1
    fi
fi

# 提示用户输入缓存目录
while true; do
    echo "请输入缓存目录:"
    read -e cache_dir
    if [[ -d $cache_dir ]]; then
        break
    else
        echo "无效的目录，请重新输入。"
    fi
done

# 提示用户输入容器名称
echo "请输入容器名称:"
read -e container_name

# 构建docker命令
docker_cmd="docker run -d \
  -v ${cache_dir}:/mnt/data/ttnode \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -v /proc:/host/proc:ro \
  --name ${container_name} \
  --hostname ttnode \
  --device /dev/mem \
  --cap-add SYS_RAWIO \
  --privileged \
  --net=host \
  --restart=always \
  -e container_name=${container_name} \
  registry.cn-hangzhou.aliyuncs.com/tiptime/ttnode:latest"

# 显示docker命令
echo "以下是完整的docker命令:"
echo $docker_cmd

# 执行docker命令
eval $docker_cmd